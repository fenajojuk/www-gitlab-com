---
release_number: "16.10" # version number - required
title: "GitLab 16.10 released with semantic versioning in the CI/CD catalog" # short title (no longer than 62 characters) - required
author: Torsten Linz # author name and surname - required
author_gitlab: tlinz # author's gitlab.com username - required
image_title: '/images/16_10/16_10-cover-image.png'
description: "GitLab 16.10 released with semantic versioning coming to the CI/CD catalog, wiki templates, the possibility to offload CI traffic to geo secondaries, new ClickHouse integration for high-performance DevOps analytics, and much more!" 
twitter_image: '/images/16_10/16_10-cover-image.png'
categories: releases # required
layout: release # required
featured: yes
rebrand_cover_img: true

# APPEARANCE
# header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
# release_number_image: "/images/X_Y/X_Y-release-number-image.svg" # uncomment if you want a svg image to replace the release number that normally overlays the background image


---

<!--
This is the release blog post file. Add here the introduction only.
All remaining content goes into data/release-posts/.

**Use the merge request template "Release-Post", and please set the calendar due
date for each stage (general contributions, review).**

Read through the Release Posts Handbook for more information:
https://about.gitlab.com/handbook/marketing/blog/release-posts/#introduction
-->
 
Today, we are excited to announce the release of GitLab 16.10 with [semantic versioning coming to the CI/CD catalog](#semantic-versioning-in-the-cicd-catalog), [wiki templates](#wiki-templates), [the possibility to offload CI traffic to geo secondaries](#offload-ci-traffic-to-geo-secondaries), [new ClickHouse integration for high-performance DevOps analytics](#new-clickhouse-integration-for-high-performance-devops-analytics), and much more!

These are just a few highlights from the 90+ improvements in this release. Read on to check out all of the great updates below.

To the wider GitLab community, thank you for the 170+ contributions you provided to GitLab 16.10!
At GitLab, [everyone can contribute](https://about.gitlab.com/community/contribute/) and we couldn't have done it without you!

To preview what's coming in next month’s release, check out our [Upcoming Releases page](/direction/kickoff/), which includes our 16.11 release kickoff video.
