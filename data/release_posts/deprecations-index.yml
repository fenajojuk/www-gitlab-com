---
'16.9':
- Rename the 'require_password_to_approve' field
- "'repository_download_operation' audit event type for public projects"
- Upgrading the operating system version of GitLab SaaS runners on Linux
- Deprecating Windows Server 2019 in favor of 2022
- Deprecate `version` field in feature flag API
- Removal of tags from small SaaS runners on Linux
- Compliance framework in general settings
- Deprecate custom role creation for group owners on self-managed
- Secure analyzers major version update
- Deprecate Python 3.9 in Dependency Scanning and License Scanning
- npm package uploads now occur asynchronously
- SAST analyzer coverage changing in GitLab 17.0
- Agent for Kubernetes option `ca-cert-file` renamed
- Deprecate `fmt` job in Terraform Module CI/CD template
- Autogenerated Markdown anchor links with dash (`-`) characters
- Scan execution policies enforcing scans with an `_EXCLUDED_ANALYZERS` variable will
  override project variables
- Support for self-hosted Sentry versions 21.4.1 and earlier
- Deprecate license metadata format V1
- Deprecate Terraform CI/CD templates
- Maven versions below 3.8.8 support in Dependency Scanning and License Scanning
- Deprecate License Scanning CI templates
- Min concurrency and max concurrency in Sidekiq options
- "`dependency_files` is deprecated"
- Security policy field `match_on_inclusion` is deprecated
- Deprecate Grype scanner for Container Scanning
- Deprecate License Scanning CI/CD artifact report type
- Dependency Scanning incorrect SBOM metadata properties
- "`omniauth-azure-oauth2` gem is deprecated"
- Heroku image upgrade in Auto DevOps build
- The `direction` GraphQL argument for `ciJobTokenScopeRemoveProject` is deprecated
'16.8':
- Dependency Scanning support for sbt 1.0.X
- "`after_script` keyword will run for cancelled jobs"
- "`metric` filter and `value` field for DORA API"
- License Scanning support for sbt 1.0.X
- Block usage of ref and sha together in `GET /projects/:id/ci/lint`
- Support for setting custom schema for backup is deprecated
- GitLab Runner provenance metadata SLSA v0.2 statement
- License List is deprecated
'16.7':
- JWT `/-/jwks` instance endpoint is deprecated
- List repository directories Rake task
- 'Dependency Proxy: Access tokens to have additional scope checks'
'16.6':
- The GitHub importer Rake task
- 'GraphQL: deprecate support for `canDestroy` and `canDelete`'
- Breaking change to the Maven repository group permissions
- Proxy-based DAST deprecated
- File type variable expansion fixed in downstream pipelines
- Legacy Geo Prometheus metrics
- Container registry support for the Swift and OSS storage drivers
'16.5':
- Offset pagination for `/users` REST API endpoint is deprecated
- Security policy field `newly_detected` is deprecated
'16.4':
- 'Geo: Legacy replication details routes for designs and projects deprecated'
- "`postgres_exporter['per_table_stats']` configuration setting"
- Deprecate change vulnerability status from the Developer role
- Internal container registry API tag deletion endpoint
- The `ci_job_token_scope_enabled` projects API attribute is deprecated
'16.3':
- Deprecate field `hasSolutions` from GraphQL VulnerabilityType
- RSA key size limits
- 'Geo: Housekeeping Rake tasks'
- Twitter OmniAuth login option is deprecated from self-managed GitLab
- GraphQL field `totalWeight` is deprecated
- Job token allowlist covers public and internal projects
- Twitter OmniAuth login option is removed from GitLab.com
'16.2':
- GraphQL field `registrySizeEstimated` has been deprecated
- OmniAuth Facebook is deprecated
- Deprecate `CiRunner` GraphQL fields duplicated in `CiRunnerManager`
- The pull-based deployment features of the GitLab agent for Kubernetes is deprecated
- Deprecated parameters related to custom text in the sign-in page
'16.10':
- "`Gitlab['omnibus_gitconfig']` configuration item is deprecated"
- Behavior change for protected variables and multi-project pipelines
- Duplicate storages in Gitaly configuration
- Hosted Runners on Linux operating system upgrade
- List container registry repository tags API endpoint pagination
- Min concurrency and max concurrency in Sidekiq options
'16.1':
- Deprecate Windows CMD in GitLab Runner
- Unified approval rules are deprecated
- Running a single database is deprecated
- Deprecate `message` field from Vulnerability Management features
- GraphQL deprecation of `dependencyProxyTotalSizeInBytes` field
'16.0':
- Changing MobSF-based SAST analyzer behavior in multi-module Android projects
- GitLab administrators must have permission to modify protected branches or tags
- GraphQL type, `RunnerMembershipFilter` renamed to `CiRunnerMembershipFilter`
- CiRunnerUpgradeStatusType GraphQL type renamed to CiRunnerUpgradeStatus
- "`sidekiq` delivery method for `incoming_email` and `service_desk_email` is deprecated"
- CiRunner.projects default sort is changing to `id_desc`
- PostgreSQL 13 deprecated
- Bundled Grafana deprecated and disabled
'15.9':
- Default CI/CD job token (`CI_JOB_TOKEN`) scope changed
- CI/CD jobs will fail when no secret is returned from Hashicorp Vault
- Legacy URLs replaced or removed
- Secure analyzers major version update
- SAST analyzer coverage changing in GitLab 16.0
- Secure scanning CI/CD templates will use new job `rules`
- Development dependencies reported for PHP and Python
- Support for Praefect custom metrics endpoint configuration
- HashiCorp Vault integration will no longer use CI_JOB_JWT by default
- Legacy Praefect configuration method
- Trigger jobs can mirror downstream pipeline status exactly
- "`omniauth-authentiq` gem no longer available"
- License-Check and the Policies tab on the License Compliance page
- Required Pipeline Configuration is deprecated
- License Compliance CI Template
- Embedding Grafana panels in Markdown is deprecated
- Queue selector for running Sidekiq is deprecated
- Managed Licenses API
- Filepath field in Releases and Release Links APIs
- GitLab Runner platforms and setup instructions in GraphQL API
- Default CI/CD job token (`CI_JOB_TOKEN`) scope changed
- Option to delete projects immediately is deprecated from deletion protection settings
- The `Project.services` GraphQL field is deprecated
- Single database connection is deprecated
- External field in Releases and Release Links APIs
- The GitLab legacy requirement IID is deprecated in favor of work item IID
- Enforced validation of CI/CD parameter character lengths
- Deprecation and planned removal for `CI_PRE_CLONE_SCRIPT` variable on GitLab SaaS
- External field in GraphQL ReleaseAssetLink type
- Old versions of JSON web tokens are deprecated
- Slack notifications integration
'15.8':
- The Visual Reviews tool is deprecated
- The latest Terraform templates will overwrite current stable templates
- Dependency Scanning support for Java 13, 14, 15, and 16
- Developer role providing the ability to import projects to a group
- The API no longer returns revoked tokens for the agent for Kubernetes
- Use of third party container registries is deprecated
- Configuring Redis config file paths using environment variables is deprecated
- "`environment_tier` parameter for DORA API"
- Cookie authorization in the GitLab for Jira Cloud app
- Non-standard default Redis ports are deprecated
- Maintainer role providing the ability to change Package settings using GraphQL API
- Container registry pull-through cache
- Live Preview no longer available in the Web IDE
- GitLab Helm chart values `gitlab.kas.privateApi.*` are deprecated
- Projects API field `operations_access_level` is deprecated
- Auto DevOps support for Herokuish is deprecated
- Deployment API returns error when `updated_at` and `updated_at` are not used together
- 'GraphQL: The `DISABLED_WITH_OVERRIDE` value of the `SharedRunnersSetting` enum
  is deprecated. Use `DISABLED_AND_OVERRIDABLE` instead'
- Auto DevOps no longer provisions a PostgreSQL database by default
- Limit personal access token and deploy token's access with external authorization
- Conan project-level search endpoint returns project-specific results
- Automatic backup upload using Openstack Swift and Rackspace APIs
- Azure Storage Driver defaults to the correct root prefix
'15.7':
- "`POST ci/lint` API endpoint deprecated"
- DAST report variables deprecation
- Support for periods (`.`) in Terraform state names might break existing states
- ZenTao integration
- DAST API scans using DAST template is deprecated
- DAST ZAP advanced configuration variables deprecation
- Shimo integration
- KAS Metrics Port in GitLab Helm Chart
- The `gitlab-runner exec` command is deprecated
- DAST API variables
- Support for REST API endpoints that reset runner registration tokens
- The Phabricator task importer is deprecated
'15.6':
- "`runnerRegistrationToken` parameter for GitLab Runner Helm Chart"
- Configuration fields in GitLab Runner Helm Chart
- GitLab Runner registration token in Runner Operator
- Registration tokens and server-side runner arguments in `gitlab-runner register`
  command
- Registration tokens and server-side runner arguments in `POST /api/v4/runners` endpoint
'15.5':
- vulnerabilityFindingDismiss GraphQL mutation
- GraphQL field `confidential` changed to `internal` on notes
- File Type variable expansion in `.gitlab-ci.yml`
'15.4':
- Non-expiring access tokens
- Starboard directive in the config for the GitLab Agent for Kubernetes
- Container Scanning variables that reference Docker
- Toggle behavior of `/draft` quick action in merge requests
- Vulnerability confidence field
'15.3':
- Security report schemas version 14.x.x
- CAS OmniAuth provider
- Use of `id` field in vulnerabilityFindingDismiss mutation
- Redis 5 deprecated
- Atlassian Crowd OmniAuth provider
'15.2':
- Remove `job_age` parameter from `POST /jobs/request` Runner endpoint
'15.10':
- Environment search query requires at least three characters
- Work items path with global ID at the end of the path is deprecated
- Legacy Gitaly configuration method
- Deprecated Consul http metrics
- DingTalk OmniAuth provider
- Major bundled Helm Chart updates for the GitLab Helm Chart
- Bundled Grafana Helm Chart is deprecated
'15.1':
- PipelineSecurityReportFinding projectFingerprint GraphQL field
- project.pipeline.securityReportFindings GraphQL query
- PipelineSecurityReportFinding name GraphQL field
- Jira DVCS connector for Jira Cloud
'15.0':
- GraphQL API legacyMode argument for Runner status
- PostgreSQL 12 deprecated
'14.9':
- GitLab self-monitoring project
- Background upload for object storage
- Integrated error tracking disabled by default
- "`user_email_lookup_limit` API field"
- Permissions change for downloading Composer dependencies
- htpasswd Authentication for the container registry
- GraphQL permissions change for Package settings
'14.8':
- Querying Usage Trends via the `instanceStatisticsMeasurements` GraphQL node
- OAuth tokens without expiration
- Test coverage project CI/CD setting
- Dependency Scanning Python 3.9 and 3.6 image deprecation
- Secure and Protect analyzer major version update
- Out-of-the-box SAST support for Java 8
- Secure and Protect analyzer images published in new location
- SAST support for .NET 2.1
- Support for gRPC-aware proxy deployed between Gitaly and rest of GitLab
- Request profiling
- Deprecate feature flag PUSH_RULES_SUPERSEDE_CODE_OWNERS
- Vulnerability Check
- Container Network and Host Security
- SAST analyzer consolidation and CI/CD template changes
- GraphQL ID and GlobalID compatibility
- GraphQL networkPolicies resource deprecated
- Deprecate legacy Gitaly configuration methods
- "`started` iteration state"
- Retire-JS Dependency Scanning tool
- Optional enforcement of SSH expiration
- "`projectFingerprint` in `PipelineSecurityReportFinding` GraphQL"
- External status check API breaking changes
- "`CI_BUILD_*` predefined variables"
- Optional enforcement of PAT expiration
- Elasticsearch 6.8
- Required pipeline configurations in Premium tier
'14.7':
- Logging in GitLab
- Monitor performance metrics through Prometheus
- Tracing in GitLab
- Sidekiq metrics and health checks configuration
- "`artifacts:reports:cobertura` keyword"
'14.6':
- apiFuzzingCiConfigurationCreate GraphQL mutation
- CI/CD job name length limit
- Legacy approval status names from License Compliance API
- bundler-audit Dependency Scanning tool
- "`type` and `types` keyword in CI/CD configuration"
'14.5':
- GraphQL API Runner status will not return `paused`
- "`pipelines` field from the `version` field"
- Package pipelines in API payload is paginated
- "`dependency_proxy_for_private_groups` feature flag"
- "`promote-to-primary-node` command from `gitlab-ctl`"
- Update to the container registry group-level API
- "`promote-db` command from `gitlab-ctl`"
- Support for SLES 12 SP2
- "`Versions` on base `PackageType`"
- Value Stream Analytics filtering calculation change
- Known host required for GitLab Runner SSH executor
- "`defaultMergeCommitMessageWithDescription` GraphQL API field"
- SaaS certificate-based integration with Kubernetes
- Changing an instance (shared) runner to a project (specific) runner
- Self-managed certificate-based integration with Kubernetes
'14.3':
- GitLab Serverless
- Legacy database configuration
- Audit events for repository push events
- OmniAuth Kerberos gem
'14.10':
- Dependency Scanning default Java version changed to 17
- Toggle notes confidentiality on APIs
- Outdated indices of Advanced Search migrations
'14.0':
- OAuth implicit grant
- Changing merge request approvals with the `/approvals` API endpoint
