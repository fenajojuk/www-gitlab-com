---
layout: markdown_page
title: "Product Direction - Global Search"
description: "GitLab supports Advanced Search for GitLab.com and Self-Managed instances. This provides users with a faster and more complete search."
canonical_path: "/direction/global-search/"
---

- TOC
{:toc}

# Global Search

| Category | Description |
| ------ |  ------ |
| [Code Search](/direction/global-search/code-search/) | Search through all your code |
| Code Navigation | Codes language intelligence. Find all references, Jump to Definition, explore all repositories |

Global Search is managed by the [Global Search Group](https://about.gitlab.com/handbook/product/categories/#global-search-group) of the [Data Stores stage](https://about.gitlab.com/direction/core_platform/#data-stores) as part of the [Core Platform Section](https://about.gitlab.com/direction/core_platform/). 

We encourage you to share feedback via [scheduling a call](https://calendly.com/bvenker-gitlab/30m-meeting-ben-venker).

## Overview

Finding anything in GitLab should be straightforward.

Global Search is the core search feature for GitLab, as the one place to search across all projects, groups, and scopes and serves to unify GitLab.

There are two modes Global Search can operate in, depending on the instance configuration:
- [Basic search](https://docs.gitlab.com/ee/user/search/#basic-search) is the default search mode for GitLab, requiring no additional configuration but providing a limited feature set.
- [Advanced search](https://docs.gitlab.com/ee/user/search/advanced_search.html) provides a richer search experience and is available at GitLab Premium self-managed, GitLab Premium SaaS, and higher tiers. It provides access to additional search scopes, advanced filters, and cross-project code search. In self-managed instances, it requires integration with [Elasticsearch](https://docs.gitlab.com/ee/integration/elasticsearch.html) or [OpenSearch](https://opensearch.org/docs/latest/).

## Vision

Our vision is to fundamentally improve how developers and teams use Global Search in GitLab. We aim to create an intuitive and efficient search experience, utilizing advancements in semantic search, smart autocomplete, and personalized search views. Our goal is to simplify the process of finding, exploring, and managing code and development data, making these tasks as straightforward and user-friendly as possible. 

This approach is central to enhancing how our users access and interact with information, enabling them to be more productive and creative in their work. As GitLab evolves, we see Global Search as a key driver of value for our users, focusing on practical and impactful improvements.

## How

**Establish Global Search as the Standard in GitLab**: We are committed to making Global Search the default search for our users. By building on a robust search platform, we can create more cohesive and effective search experiences, moving away from fragmented features. This approach not only enhances user satisfaction but also opens up avenues for monetizing advanced features.

**Transforming Advanced Search**: Our current Advanced Search needs a reevaluation from the user's perspective. Today, it offers basic app-wide searching with limited filters and scopes, which doesn't truly qualify as 'advanced' compared to industry standards. We propose a shift in thinking: Advanced Search shouldn't just be a premium layer of the existing search but a platform for innovative, distinct features that add real value. Features like code search and semantic search, for instance, could stand out as unique offerings, enhancing user experience and creating new revenue streams.

### Make Global Search the default search for GitLab

**Rethinking Default Search**: Currently, GitLab's basic search offers limited functionality and performance. In today's competitive landscape, restricting a robust global search feature to a paid tier is untenable. Notably, a significant majority of our paid self-managed customers don't run their own search clusters, resulting in a subpar search experience for paying customers compared to rivals like GitHub and Jira.

**Integrated Search Engine Solution**: Incorporating a search engine directly within GitLab simplifies the user experience by eliminating the need to run separate search clusters outside of GitLab. This integration paves the way for a unified search API, streamlining the search experience across GitLab and enhancing the quality of search features available to all users.
**Broadening Content Scope in Search**: A key area of advancement will be making it simpler to index more content types in our search capabilities, such as vulnerabilities and pipelines.

#### Enhancing default search capabilities

**Autocomplete Suggestions**: Implement intelligent, dynamic autocomplete suggestions to accelerate the search process, further enriched by semantic search.

**Expanding Search Filters**: Broaden the range of search filters beyond what is currently available in Advanced Search, applying them across more search scopes for a more refined search experience.

**Saved Searches**: Introduce the ability to save searches and filters, a feature prevalent among our competitors. This functionality is particularly beneficial for team leads, project managers, and engineering managers, laying the groundwork for future enhancements like data visualizations and dashboards.

### Transforming Advanced Search

As we evolve Global Search into a default feature, our focus shifts to integrating revenue-generating capabilities into the search. Recognizing that our current Advanced Search doesn't meet industry standards, we are committed to driving innovation that delivers real value to our users.

#### Code search

In the dynamic world of software development, code complexity and volume are ever-increasing challenges. A staggering [74% of development teams](https://drive.google.com/file/d/19KOx60XOf9EO4YvssLXMh-FqTmiHHHpd/edit?disco=AAAAa0ILCBc) hesitate to make changes due to fear of unintended consequences. Our enhanced Code Search aims to alleviate this by breaking down complex codebases into manageable segments, facilitating easier understanding and improvements.

#### Semantic search powered by embeddings

Semantic search can significantly enhance the way users interact with textual content on GitLab, such as issues, projects, comments, and merge requests. By grasping the context and meaning behind search queries, semantic search delivers more relevant and precise results, extending the limits of traditional keyword-based searches.
This capability is particularly advantageous for navigating complex discussions across issues, comments, and merge requests, enabling users to quickly find pertinent information and insights. As a result, it streamlines workflow processes, cuts down on the time spent sifting through project artifacts, and fosters a more efficient and user-centric search experience. This not only boosts productivity but also enhances collaboration by connecting team members with the information they need more intuitively and effectively.

##### Embeddings storage and APIs

The power of semantic search will be augmented by our initiative to build, manage, and expose embeddings. These embeddings will not only enrich the search experience but also be instrumental across GitLab's ecosystem. For instance, they can be utilized in Duo chat within a Retrieval Augmented Generation (RAG) architecture, providing contextual relevance to Large Language Models(LLMs). This integration ensures that our search capabilities are not just advanced but also deeply interconnected with the overall functionality of GitLab.
