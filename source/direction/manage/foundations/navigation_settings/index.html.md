---
layout: markdown_page
title: "Category Direction - Navigation & Settings"
description: "This page outlines the direction for the navigation and settings product category."
canonical_path: "/direction/manage/foundations/navigation_settings/"
---

- TOC
{:toc}

## Navigation & Settings

|                       |                               |
| -                     | -                             |
| Stage                 | [Manage](/direction/manage/)  |
| Maturity              | N/A |
| Content Last Reviewed | `2024-01-09`                  |

### Introduction and how you can help

Thanks for visiting this direction page on the Navigation & Settings category at GitLab. This page belongs to the [Foundations Group](https://about.gitlab.com/handbook/product/categories/#foundations-group) within the Manage Stage and is maintained by [Jeff Tucker](https://gitlab.com/jtucker_gl), but everyone can contribute. If you're a GitLab user and have direct feedback about your needs for Navigation, we'd especially love to hear from you.

* Share feedback in a related [issue](https://gitlab.com/groups/gitlab-org/-/issues/?sort=weight&state=opened&label_name%5B%5D=Category%3ANavigation%20%26%20Settings&first_page_size=20) or [epic](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name%5B%5D=Category%3ANavigation%20%26%20Settings), or [on a video call](https://calendly.com/jtucker-gitlab/30min)
* To contribute, please [open an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new#) using the `~"Category:Navigation & Settings"` label
* Team members can reach us in the `#g_manage_foundations` Slack channel.

### Overview

This joint category is shared between the navigation elements of GitLab and the settings that make it configurable. These aspects serve as the framing for the product itself. Our goal is to make these areas of the product more useful, predictable, and convenient.

Navigation is the highway through which almost every feature is accessed or discovered, and it must be accessible for all types of people and abilities. For this reason, the navigation structure is one of the most important parts of any application’s user interface.

Settings provide administrators, owners, and users an opportunity to configure GitLab to their unique needs. We default to [convention over configuration](https://handbook.gitlab.com/handbook/product/product-principles/#convention-over-configuration) but also provide flexibility to customize settings if needed.

### Strategy and Themes

The complexity and breadth of GitLab weighs heavily on both the navigation and settings. In FY24 we invested in [overhauling our core navigation](https://gitlab.com/groups/gitlab-org/-/epics/9044 "Overhaul the core navigation experience"), and now we are looking towards how the navigation must evolve to address existing pain points while also accommodating future changes to the product. With settings we must address the historical challenges: discoverability, consistency, access, and visibility.

#### Navigation

The navigation experience _must_ be described as intuitive. When navigation is effective, users trust that they can rely on it to help orient and empower themselves within the product. To aid in decision making we will continue to invest in these themes:

- Minimizing the feeling of being overwhelmed
- Orienting users across the platform
- Making it easier to pick up where you left off

Executing this plan we will requires iterating in three ways: 

1. Addressing insights from our quarterly navigation survey
1. Simplifying the navigation structure by removing and relocating outdated items
1. Refining the quality and polish of the experience to be more useful, functional, and intuititve

In addition, the Foundations Group owns the [navigation structures](https://handbook.gitlab.com/handbook/product/ux/navigation/) of GitLab and is responsible for reviewing and approving any proposed changes. Given the effort required by these requests we dedicate time each milestone to support teams with [this process](https://handbook.gitlab.com/handbook/product/ux/navigation/#how-to-propose-a-change-that-impacts-navigation).

#### Settings

Over years of product development, the Gitlab settings have evolved into a catchall space lacking a cohesive structure. As a result, it's evident there is difficulty in discovering, finding, and configuring settings within GitLab. Our goals to address this include:  
- Improving the information architecture of the settings to aid discovery
- Aligning UI elements and patterns to our design system for a consistent and reliable user experience
- Enhancing functionality to help users stay on task 


**Taking inventory**

Our starting point is to perform an assessment of the current experience. This includes blending our findings with the extensive [collection of epics ](https://gitlab.com/groups/gitlab-org/-/epics/4410 "GitLab Settings Prioritization & Scope") based on historical requests and user research. There are likely straightforward opportunities that require minimal validation. We will prioritize these earlier in our backlog while delving deeper into other areas.

**Unifying feature development and the design system**

The Foundations Group is also responsible for the [Design system category](https://about.gitlab.com/direction/manage/foundations/design_system/). Previously treated as a separate focus area, our strategy now is to bundle product improvements to the Gitlab product and design system into a cohesive package of work. We aim to deliver iterative improvements to the design system that will streamline our work to improve the settings experience. 

### Maturity Plan

As this is not a marketing category, we don't have a specific measure of maturity.

### Target Audience

All [roles & personas](https://about.gitlab.com/handbook/product/personas/) interact with this category in some capacity.