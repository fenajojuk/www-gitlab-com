---
layout: markdown_page
title: "Stage Direction - AI-powered"
description: "AI-powered workflows boost efficiency and reduce cycle times with the help of AI."
canonical_path: "/direction/ai-powered/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

GitLab is embracing AI/ML technologies within the software development lifecycle, as the most comprehensive AI-powered DevSecOps platform. Since mid 2021 we've been leverage AI/ML technologies to enrich features on our platform. We're calling these AI-powered capabilities, [GitLab Duo](https://about.gitlab.com/gitlab-duo/). 

The name GitLab Duo is rooted in You + GitLab AI = the AI dynamic duo. GitLab Duo goes beyond just being an AI pair programmer: It is an expanding toolbox of features integrated into the DevSecOps Platform to help teams across the entire software development environment become more efficient. 

Duo improves team collaboration and reduces the security and compliance risks of AI adoption by bringing the entire software development lifecycle into a single AI-powered application. At GitLab, we believe everyone can contribute. By bringing GitLab Duo capabilities to every persona who uses GitLab, everyone can benefit from AI-powered workflows and organizations can ship secure software faster.

<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/LifJdU3Qagw?si=A4kl6d32wPYC4168" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</figure>

### Goal

As we continue building GitLab Duo, there are some high level goals that drive our development process. 

AI-powered workflows
* Harness the power of AI to enhance to boost efficiency and reduce cycle times in [every phase of the software development lifecycle](https://about.gitlab.com/platform/).
Privacy-first
* GitLab [we ensures AI model vendors don't use customer data to train their models](https://docs.gitlab.com/ee/user/ai_features.html#training-data).
Transparent
* Our [publicly available documentation](https://docs.gitlab.com/ee/user/ai_features.html) describes all AI models used by GitLab Duo and understand exactly how your code base is utilized.
Best AI Model powering each capability
* We aren't in exclusive agreements with any one AI vendor. We use the best AI model for each task. 
AI in all distributions
* While we innovate with AI first on GitLab.com, our multitenant SaaS platform; we're committed to bringing Duo capabilities to [all GitLab distributions](https://about.gitlab.com/install/) including self-managed and Dedicated instances. 

## Groups 
The AI-powered stage consists of the following product led groups: 

- [AI Framework](/direction/ai-powered/ai_framework): Building common tools to support GitLab teams integrating AI/ML technologies into GitLab features. 
- [AI Model Validation](/direction/ai-powered/ai_model_validation): Research and Evaluate AI/ML models for use to enrich the GitLab product with Generative AI.
- [Duo Chat](/direction/ai-powered/duo_chat): GitLab's AI-powered DevSecOps assistant. 
- [Custom Models](/direction/ai-powered/custom_models):Focused on grounding and enriching AI models with Customer data to produce higher quality model outputs

## What's Next & Why

We're actively building many AI-powered features into GitLab Duo, some of our upcoming milestones include: 

- [Chat Beta](https://about.gitlab.com/blog/2023/11/09/gitlab-duo-chat-beta/)
- Model Evaluation at Scale 
- New Duo Feature Research

## What we've recently completed

We've made a lot of progress in 2023, introducing [GitLab Duo](https://about.gitlab.com/gitlab-duo/) and are quickly building to keep up with the rapid innovations in the wider AI/ML market. 

### 2023
You can keep up with our latest releases by following our [AI Features documentation](https://docs.gitlab.com/ee/user/ai_features.html)

Major achievements include: 

- [Code Suggestions Experimental, Beta, and GA release]([../create/code_creation/code_suggestions/#short-term-strategy](https://about.gitlab.com/solutions/code-suggestions/))
- Experimental and Beta release of [Duo Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html)
- Experimental and Beta release of 15 Generative AI capabilities that we're calling [GitLab Duo](https://about.gitlab.com/gitlab-duo/). Read about these capabilities in our [April AI Fireside chat](https://about.gitlab.com/blog/2023/05/03/gitlab-ai-assisted-features/)
- Initial design and implementation of our [AI Architecture](https://docs.gitlab.com/ee/user/ai_features.html) to support Generative AI use cases. 

### 2022
- GA of Suggested Reviewers
- Experimental Code Suggestions

### 2021
- Established the ModelOps stage (later split to create the AI-powered stage)
- [Acquisition of UnReview](#smarter-code-reviewer-suggestions)
- Internal beta of [Automatic Issue Labeling](#automatic-issue-labeling)

#### Smarter Code Reviewer suggestions

> [GitLab acquires UnReview as it looks to bring more ML tools to its platform](https://techcrunch.com/2021/06/02/gitlab-acquires-unreview-as-it-looks-to-bring-more-ml-tools-to-its-platform/)

Integrating [UnReview’s technology](https://about.gitlab.com/press/releases/2021-06-02-gitlab-acquires-unreview-machine-learning-capabilities.html) into the GitLab platform marks our first step in building GitLab’s AI Assisted features for DevOps.

#### Automatic Issue Labeling
We are currently actively working on an ML model that automatically labels GitLab internal issues based on issue content. [You'll see GitLab issues](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name[]=automation%3Aml) with the `automation:ml` label that have been automatically labeled by our model. You can also provide training feedback to the model if it is incorrect by applying the `automation:ml wrong` label. GitLab team members can view a feed of these issues with probability data in Slack in the [#feed-tanuki-stan](https://gitlab.slack.com/archives/C01HM3RA2RE) channel.

We pursued this feature first as a way to get a data science workload working within GitLab's existing CI/CD as well as running on top of production GitLab data and interacting with the GitLab data model. This will set the foundation for work in our [MLOps group](../mlops) and our other AI Assisted categories listed above.

### What We're Not Doing
* Air-Gapped Environments - Many advanced data science use cases today exist within government and high-security environments. These environments pose many complex challenges like accessing data and compute resources. Our focus for the next year will be on our .com SaaS offering to bring ModelOps capabilities to as many customers as quickly as possible. We will aim to support self-hosted GitLab environments and eventually air-gapped environments, but it is not our primary focus today.

## Analyst Landscape

Based on [GitLab’s 2023 What's Next in DevSecOps survey,](https://about.gitlab.com/developer-survey/#ai)  
* AI is becoming more and more embedded in software development.
  * The vast majority (83%) of respondents agreed that it is essential to implement AI in their software development processes to avoid falling behind, and this was consistent across development, operations, and security
* Organizations are using AI across the software development lifecycle — and that means more than code generation.
  * Developers spend 25% of their time writing new code. That means three quarters of developers’ time is taken up with other tasks beyond code generation.
* Respondents expressed concerns around data privacy, intellectual property, and security.
  * There is concern around introducing AI into the software development lifecycle, but it’s not extreme: 32% of respondents were “very” or “extremely” concerned about AI being introduced into the software development lifecycle, while 23% were “not very” or “not at all” concerned. The largest proportion, 45%, were “somewhat concerned”


These statistics validate the importance of [GitLab’s AI Assisted features for DevOps](https://about.gitlab.com/direction/modelops/ai_assisted/), and integrating automation and machine learning technology like UnReview into the GitLab platform.

Industry analyst research into successful operationalization of machine learning outlines the many challenges organizations face by adopting point solution technologies. This is contrasted with the business value provided by integrating AI Assisted features, DataOps, MLOps, and ModelOps into existing DevOps processes.

> "With the rapid increase in cloud adoption, spurred by the COVID-19 pandemic, we’re seeing increased demand for cloud-enabled DevOps solutions," said Jim Mercer, research director DevOps and DevSecOps at IDC. "DevOps teams who can capitalize on cloud solutions that provide innovative technologies, such as machine learning, to remove friction from the DevOps pipeline while optimizing developer productivity are better positioned to improve code quality and security driving improved business outcomes."

* Forrester - [State of TuringBots, 2023](https://www.forrester.com/report/the-state-of-turingbots-2023/RES179746
  * Accompanying blog post: []"AI And Generative AI For The Software Development Lifecycle"](https://www.forrester.com/blogs/ai-and-generative-ai-in-the-software-development-lifecycle/)
* Forrester - [Future of TuringBots, 2023](https://www.forrester.com/report/the-future-of-turingbots/RES179600)
* Gartner - [What is Generative AI](https://www.gartner.com/en/topics/generative-ai)
* Gartner - [Tackling Trust, Risk and Security in AI Models](https://www.gartner.com/en/articles/what-it-takes-to-make-ai-safe-and-effective)
* McKinsey - [The economic potential of generative AI: The next productivity frontier](https://www.mckinsey.com/capabilities/mckinsey-digital/our-insights/the-economic-potential-of-generative-ai-the-next-productivity-frontier#introduction)
* [Retool's 2023 State of AI in Production](https://retool.com/reports/state-of-ai-2023)

### Internal resources
GitLab team members can learn more in our internal handbook: 
* Slack: #ai_strategy
* [AI Strategy](https://internal.gitlab.com/handbook/product/ai-strategy/)

<p align="center">
    <i><br>
    Last Reviewed: 2024-01-30<br>
    Last Updated: 2024-01-30
    </i>
</p>
